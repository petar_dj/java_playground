package org.playground.model;

import java.util.ArrayList;
import java.util.List;

public class Employee {
    public int empNo;
    public String birthDate;
    public String firstName;
    public String lastName;
    public String gender;
    public String hireDate;
    public List<Salaries> salaries = new ArrayList<>();

    public Employee(int empNo, String birthDate, String firstName, String lastName, String gender, String hireDate) {
        this.empNo = empNo;
        this.birthDate = birthDate;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.hireDate = hireDate;
    }

    public void setSalaries(List<Salaries> salaries) {
        this.salaries.addAll(salaries);
    }

    public int getEmpNo() {
        return empNo;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "empNo=" + empNo +
                ", birthDate='" + birthDate + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", hireDate='" + hireDate + '\'' +
                '}';
    }
}
