package org.playground.model;

public class Salaries {

    int empNumber;
    int salary;
    String fromDate;
    String toDate;
    public Salaries(int empNumber, int salary, String fromDate, String toDate) {
        this.empNumber = empNumber;
        this.salary = salary;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public int getEmpNumber() {
        return empNumber;
    }

    @Override
    public String toString() {
        return "Salaries{" +
                "empNumber=" + empNumber +
                ", birthDate='" + salary + '\'' +
                ", firstName='" + fromDate + '\'' +
                ", lastName='" + toDate + '\'' +
                '}';
    }
}
