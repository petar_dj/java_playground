package org.playground.service;

import org.playground.model.Employee;
import org.playground.model.Salaries;
import org.playground.repository.EmployeeRepoJDBC;
import org.playground.repository.SalariesRepoMock;

import java.sql.Connection;
import java.util.*;
import java.util.stream.Collectors;

public class BusinessService {

    EmployeeRepoJDBC employeeRepoJDBC;
    SalariesRepoMock salariesRepoMock;


    public BusinessService(Connection connection) {
        employeeRepoJDBC = new EmployeeRepoJDBC(connection);
        salariesRepoMock = new SalariesRepoMock();
    }

    public List<Employee> getEmployeesWithSalaries() {
        List<Employee> employees = employeeRepoJDBC.getEmployees();
        List<Salaries> salaries = salariesRepoMock.getSalaries();

        HashMap<Integer, List<Salaries>> grouped = new HashMap<>();
        for (Salaries s : salaries) {
            List<Salaries> found = grouped.get(s.getEmpNumber());

            if (Objects.isNull(found)) {
                ArrayList<Salaries> initial = new ArrayList<>();
                initial.add(s);
                grouped.put(s.getEmpNumber(), initial);
            } else {
                found.add(s);
            }
        }

        Map<Integer, List<Salaries>> functionalGrouped = salaries.stream().collect(Collectors.groupingBy(Salaries::getEmpNumber));

        for (Employee e : employees) {
            List<Salaries> empSalaries = functionalGrouped.get(e.getEmpNo());

            if(Objects.isNull(empSalaries)){
                e.setSalaries(new ArrayList<>());
            }   else {
                e.setSalaries(empSalaries);
            }
        }

//        employees.stream().map(e -> {
//            List<Salaries> empSalaries = functionalGrouped.get(e.getEmpNo());
//            if(Objects.isNull(empSalaries)){
//                e.setSalaries(new ArrayList<>());
//            }   else {
//                e.setSalaries(empSalaries);
//            }
//            return e;
//        });

//        employees.forEach(e -> {
//            List<Salaries> empSalaries = functionalGrouped.get(e.getEmpNo());
//            if(Objects.isNull(empSalaries)){
//                e.setSalaries(new ArrayList<>());
//            }   else {
//                e.setSalaries(empSalaries);
//            }
//        });

        return employees;
    }

}

/*
   e1  s1    --> e1 [s1, s2]
   e2 s2     --> e2 [s1, s2]
   e1  s2
   e2 s1

 */