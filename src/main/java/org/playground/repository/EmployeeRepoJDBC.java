package org.playground.repository;

import org.playground.model.Employee;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeRepoJDBC implements EmployeeRepo {

    Connection connection;

    public EmployeeRepoJDBC(final Connection connection){
        this.connection = connection;
    }

    public List<Employee> getEmployees(){
        ArrayList<Employee> employees = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from employees limit 10");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                System.out.println(resultSet.getInt("emp_no")
                        + " " + resultSet.getString("birth_date")
                        + " " + resultSet.getString("first_name")
                        + " " + resultSet.getString("last_name")
                        + " " + resultSet.getString("gender")
                        + " " + resultSet.getString("hire_date")
                );

                Employee employee = new Employee(
                        resultSet.getInt("emp_no"),
                        resultSet.getString("birth_date"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("gender"),
                        resultSet.getString("hire_date")
                );

                employees.add(employee);
            }

            employees.forEach(e -> {
                System.out.println(e.toString());
            });

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return employees;
    }

    public Employee getEmployeeByEmpNo(final int empNo){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from employees where emp_no = ?");
            preparedStatement.setInt(1, empNo);

            ResultSet resultSet = preparedStatement.executeQuery();
            Employee employee = null;
            while (resultSet.next()) {
                System.out.println(resultSet.getInt("emp_no")
                        + " " + resultSet.getString("birth_date")
                        + " " + resultSet.getString("first_name")
                        + " " + resultSet.getString("last_name")
                        + " " + resultSet.getString("gender")
                        + " " + resultSet.getString("hire_date")
                );

                 employee = new Employee(
                        resultSet.getInt("emp_no"),
                        resultSet.getString("birth_date"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("gender"),
                        resultSet.getString("hire_date")
                );

            }

            return employee;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }
}
