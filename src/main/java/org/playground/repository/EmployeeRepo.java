package org.playground.repository;

import org.playground.model.Employee;

import java.util.List;

public interface EmployeeRepo {

    List<Employee> getEmployees();

    Employee getEmployeeByEmpNo(final int empNo);
    
}
