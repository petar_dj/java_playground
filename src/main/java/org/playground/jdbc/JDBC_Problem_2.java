package org.playground.jdbc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.cdimascio.dotenv.Dotenv;
import org.playground.model.Employee;
import org.playground.repository.EmployeeRepoJDBC;
import org.playground.service.BusinessService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class JDBC_Problem_2 {

    public static Dotenv dotenv = Dotenv.load();
    static ObjectMapper mapper = new ObjectMapper();

    public static void main(String[] args) throws SQLException, JsonProcessingException {
        final String postgresUser = dotenv.get("POSTGRES_USER");
        final String postgresPass = dotenv.get("POSTGRES_PASS");
        final String postgresUrl = dotenv.get("POSTGRES_URL");

        System.out.println(postgresUser);
        System.out.println(postgresPass);

        Connection conn = DriverManager.getConnection(postgresUrl, postgresUser, postgresPass);

        BusinessService businessService = new BusinessService(conn);

        List<Employee> employees = businessService.getEmployeesWithSalaries();

        String employeesJson = mapper.writeValueAsString(employees);

        System.out.println(employeesJson);
    }
}
