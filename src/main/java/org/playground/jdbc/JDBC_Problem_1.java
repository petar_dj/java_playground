package org.playground.jdbc;

import io.github.cdimascio.dotenv.Dotenv;
import org.playground.model.Employee;
import org.playground.repository.EmployeeRepoJDBC;

import java.sql.*;
import java.util.List;

public class JDBC_Problem_1 {

    public static Dotenv dotenv = Dotenv.load();

    public static void main(String[] args) throws SQLException {
        final String postgresUser = dotenv.get("POSTGRES_USER");
        final String postgresPass = dotenv.get("POSTGRES_PASS");
        final String postgresUrl = dotenv.get("POSTGRES_URL");

        System.out.println(postgresUser);
        System.out.println(postgresPass);

        Connection conn = DriverManager.getConnection(postgresUrl, postgresUser, postgresPass);

        EmployeeRepoJDBC employeeRepoJDBC = new EmployeeRepoJDBC(conn);

        List<Employee> employees = employeeRepoJDBC.getEmployees();

        employees.forEach(employee -> employee.toString());

        Employee employee = employeeRepoJDBC.getEmployeeByEmpNo(3);
    }
}
